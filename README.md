### 3D Packing JS Lib

Source of the library is present in src with qpacking.js as the entry point. It exports the QuifersPacking module.

Developement steps:-
1. Clone the repository 
2. npm install
3. edit source in the /src directory
4. npm run build
5. test the code in the browser with index.html

Usage Steps:-
1. import QuifersPacking from "src/qpacking.js" and use it as required.
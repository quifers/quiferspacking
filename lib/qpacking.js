"use strict";

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "./three", "./BufferGeometryUtils", "./TrackballControls"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("./three"), require("./BufferGeometryUtils"), require("./TrackballControls"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.three, global.BufferGeometryUtils, global.TrackballControls);
    global.undefined = mod.exports;
  }
})(void 0, function (exports, _three) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.QuifersPacking = undefined;

  var THREE = _interopRequireWildcard(_three);

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function () {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};

    if (obj != null) {
      var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
          var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

          if (desc && (desc.get || desc.set)) {
            Object.defineProperty(newObj, key, desc);
          } else {
            newObj[key] = obj[key];
          }
        }
      }
    }

    newObj.default = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  var hashCode = function hashCode(s) {
    var hexColors = [0x3CB9E7, 0x5B42B3, 0xE1AD78, 0x479306, 0x86A088, 0x7C4253, 0xABEEEA, 0x2DCCA4, 0x5FBE3C, 0x5B3FBC, 0x829D5F, 0x00AFF8, 0xA156E0, 0xB4F732, 0x48E889, 0x40CDD2, 0xDE27D1, 0x0835F2, 0x6A6529, 0x8627EA, 0x03BD19, 0x0E7255, 0x45D8BC, 0xA17079, 0xA00C7E, 0x782263, 0xB61287, 0x8DD558, 0x2FAA01, 0x3A1479, 0x3E2508, 0x8CEAF7, 0x175FCA, 0x7FA19A, 0x3D552C, 0xE5A1AB, 0xF0D4FA, 0xF7AA84, 0x4B1913, 0x609014, 0xBA8326, 0x926776, 0x27DC15, 0x0512F3, 0x653F64, 0x32EB51, 0x2F496B, 0xF93C54, 0x26A489, 0x7EFAE6, 0xE28DEE, 0xF0D394, 0xF9D263, 0x6990D2, 0xA51AFF, 0x0E19CF, 0x8B06FA, 0xBE391A, 0xC83680, 0x836AE6, 0x9F4B91, 0xB1CB7C, 0x53B9CC, 0x3846FD, 0x781D2F, 0x3CE877, 0x9296D1, 0x234DC0, 0xAF9F68, 0xCD0EB3, 0xEEFC67, 0x4F9E57, 0xF8B367, 0x307CFD, 0x5CB6B6, 0xA9496B, 0x1ABB81, 0x289DBF, 0x5F0CCF, 0xA2C587, 0xC201C6, 0xCD178A, 0x8B229E, 0x1EE1BA, 0x16CEC5, 0x588694, 0xD62EDB, 0x966C80, 0xAAE9CA, 0x3D00CC, 0x615E8E, 0x355642, 0x1E98A2, 0x85C6E8, 0x126132, 0xE9438C, 0x2B2727, 0xA353BF, 0x5342A3];
    return hexColors[s];
  };

  function QuifersPacking(dataObj, container, gapValue, isWall, dimension) {
    //if (typeof truckNum === "undefined") {
    //	truckNum = 0;
    //}
    this.container = container;
    this.dimension = dimension;
    this.gapValue = gapValue;
    this.isWall = isWall;
    this.data_array = [];
    var temp_array = [];
    this.data_array = [];
    var data = dataObj || [];

    for (var i = 0; i < data.length; i++) {
      var input = data[i];
      temp_array = [input.x, input.y, input.z, input.w, input.h, input.l, input.id, input.hexcode];
      this.data_array.push(temp_array);
    }

    this.camera = null;
    this.controls = null;
    this.scene = null;
    this.renderer = null;
    this.pickingData = [];
    this.pickingTexture = null;
    this.pickingScene = null;
    this.highlightBox = null;
    this.colorCode = [];
    this.mouse = new THREE.Vector2();
    this.offset = new THREE.Vector3(10, 10, 10);
    this.init(this.isWall, this.dimension);
    this.boundAnimate = this.animate.bind(this);
    this.animate();
  } //  destroys old webgl context


  QuifersPacking.prototype.destroy = function () {
    if (this.renderer) {
      this.renderer.forceContextLoss();
      this.renderer.context = null;
      this.renderer.domElement = null;
      this.renderer = null;
    }
  };

  QuifersPacking.prototype.init = function (isWall, dimension) {
    //console.log("isWall", isWall)
    var vehicle_width = dimension.breadth; //244;//

    var vehicle_height = dimension.height; //244;//

    var vehicle_length = dimension.length + 2; //548.6;//

    this.camera = new THREE.PerspectiveCamera(50, this.container.getBoundingClientRect().width / this.container.getBoundingClientRect().height, 1, 1000); //isWall = true

    if (isWall == true) {
      this.camera.position.z = 7.5 * 30 / (this.container.getBoundingClientRect().width / window.innerWidth);
    } else {
      this.camera.position.x = -300 / (this.container.getBoundingClientRect().width / window.innerWidth);
    }

    this.controls = new THREE.TrackballControls(this.camera, this.container);
    this.controls.rotateSpeed = 1.0;
    this.controls.zoomSpeed = 1.2;
    this.controls.panSpeed = 0.8;
    this.controls.noZoom = false;
    this.controls.noPan = false;
    this.controls.staticMoving = true;
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xffffff);
    this.scene.add(new THREE.AmbientLight(0x555555));
    var light = new THREE.SpotLight(0xffffff, 1.5);
    var light2 = new THREE.SpotLight(0xffffff, 1.5);

    if (isWall == false) {
      light.position.set(-7000, -7000, -7000);
      light2.position.set(3500, 3500, 3500);
    } else {
      light.position.set(7000, 7000, 7000);
      light2.position.set(-3500, -3500, -3500);
    }

    this.scene.add(light);
    this.scene.add(light2);
    var defaultMaterial = new THREE.MeshPhongMaterial({
      color: 0xffffff,
      flatShading: false,
      vertexColors: THREE.VertexColors,
      shininess: 0
    });

    function applyVertexColors(geometry, color) {
      var position = geometry.attributes.position;
      var colors = [];

      for (var i = 0; i < position.count; i++) {
        colors.push(color.r, color.g, color.b);
      }

      geometry.addAttribute('color', new THREE.Float32BufferAttribute(colors, 3));
    }

    var geometriesDrawn = [];
    var geometriesPicking = [];
    var matrix = new THREE.Matrix4();
    var quaternion = new THREE.Quaternion(); //var vehicle_width = 8

    var color = new THREE.Color();
    var new_color_code = 0;
    var color_code_list = [];

    for (var i = 0; i < this.data_array.length; i++) {
      var geometry = new THREE.BoxBufferGeometry();
      var position = new THREE.Vector3();
      var leftShift = this.container.clientHeight / 10;
      var upShift = this.container.clientWidth / 15;

      if (isWall == false) {
        position.x = this.data_array[i][0] - this.data_array[0][0];
        position.y = this.data_array[i][1] - this.data_array[0][1] - upShift;
        position.z = this.data_array[i][2] - this.data_array[0][2] - leftShift;
      } else {
        position.x = this.data_array[i][0] - this.data_array[0][0] - leftShift;
        position.y = this.data_array[i][1] - this.data_array[0][1] - upShift;
        position.z = this.data_array[i][2] - this.data_array[0][2];
      }

      var scale = new THREE.Vector3();
      scale.x = this.data_array[i][3] - this.gapValue / 100;
      scale.y = this.data_array[i][4] - this.gapValue / 100;
      scale.z = this.data_array[i][5] - this.gapValue / 100;
      matrix.compose(position, quaternion, scale);
      geometry.applyMatrix(matrix);
      geometry.translate(this.data_array[i][3] / 2, this.data_array[i][4] / 2, this.data_array[i][5] / 2);
      var edges = new THREE.EdgesGeometry(geometry);
      var line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line); //console.log("color code list", color_code_list)
      // give the geometry's vertices a random color, to be displayed
      //console.log(this.data_array[i][7].toString(16))

      applyVertexColors(geometry, color.setHex(this.data_array[i][7]));
      geometriesDrawn.push(geometry);
      var sku_color = color.setHex(this.data_array[i][7]);
      sku_color = sku_color.getHexString(); // save color hexstring for caller

      this.colorCode.push([this.data_array[i][6], sku_color]);
    }

    console.log(isWall);

    if (isWall == false) {
      var geometry_t = new THREE.BoxBufferGeometry();
      var position_t = new THREE.Vector3();
      position_t.x = this.data_array[0][0] + vehicle_width / 2;
      position_t.y = this.data_array[0][1] + vehicle_height / 2 - upShift;
      position_t.z = this.data_array[0][2] - 2 - leftShift;
      var scale_t = new THREE.Vector3();
      scale_t.x = vehicle_width + 1;
      scale_t.y = vehicle_height + 1;
      scale_t.z = vehicle_width / 10;
      matrix.compose(position_t, quaternion, scale_t);
      geometry_t.applyMatrix(matrix);
      applyVertexColors(geometry_t, color.setHex(0xFFFF00));
      geometriesDrawn.push(geometry_t);
      var edges_t = new THREE.EdgesGeometry(geometry_t);
      var line_t = new THREE.LineSegments(edges_t, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line_t);
      var geometry_t1 = new THREE.BoxBufferGeometry();
      var position_t1 = new THREE.Vector3();
      position_t1.x = position_t.x;
      position_t1.y = position_t.y + 0.5 * position_t.y + upShift; // - this.container.getBoundingClientRect().height)

      position_t1.z = position_t.z - scale_t.z * 2 - 2 - leftShift;
      var scale_t1 = new THREE.Vector3();
      scale_t1.x = scale_t.x; // Math.random() * 200 + 100;

      scale_t1.y = scale_t.y * 0.50; //Math.random() * 200 + 100;

      scale_t1.z = scale_t.z * 4;
      matrix.compose(position_t1, quaternion, scale_t1);
      geometry_t1.applyMatrix(matrix);
      applyVertexColors(geometry_t1, color.setHex(0xA9A9A9));
      geometriesDrawn.push(geometry_t1);
      var edges_t1 = new THREE.EdgesGeometry(geometry_t1);
      var line_t1 = new THREE.LineSegments(edges_t1, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line_t1);
      var geometry_t2 = new THREE.BoxBufferGeometry();
      var position_t2 = new THREE.Vector3();
      position_t2.x = position_t1.x;
      position_t2.y = position_t1.y - scale_t1.y;
      position_t2.z = position_t1.z - (scale_t1.z * 1.5 - scale_t1.z) / 2;
      var scale_t2 = new THREE.Vector3();
      scale_t2.x = scale_t1.x; // Math.random() * 200 + 100;

      scale_t2.y = 0.5 * scale_t.y; //Math.random() * 200 + 100;

      scale_t2.z = scale_t1.z * 1.5;
      matrix.compose(position_t2, quaternion, scale_t2);
      geometry_t2.applyMatrix(matrix);
      applyVertexColors(geometry_t2, color.setHex(0xD3D3D3));
      geometriesDrawn.push(geometry_t2);
      var edges_t2 = new THREE.EdgesGeometry(geometry_t2);
      var line_t2 = new THREE.LineSegments(edges_t2, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line_t2);
      var geometry_t3 = new THREE.BoxBufferGeometry();
      var position_t3 = new THREE.Vector3();
      position_t3.x = position_t1.x;
      position_t3.y = position_t2.y - scale_t2.y / 2 - 15;
      position_t3.z = position_t1.z - scale_t1.z / 2;
      var scale_t3 = new THREE.Vector3();
      scale_t3.x = scale_t1.x; // Math.random() * 200 + 100;

      scale_t3.y = 30; //Math.random() * 200 + 100;

      scale_t3.z = scale_t1.z * 2;
      matrix.compose(position_t3, quaternion, scale_t3);
      geometry_t3.applyMatrix(matrix);
      applyVertexColors(geometry_t3, color.setHex(0x000000));
      geometriesDrawn.push(geometry_t3);
      var edges_t3 = new THREE.EdgesGeometry(geometry_t3);
      var line_t3 = new THREE.LineSegments(edges_t3, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line_t3);
      var geometry_t4 = new THREE.BoxBufferGeometry();
      var position_t4 = new THREE.Vector3();
      position_t4.z = position_t3.z - 500;
      position_t4.x = position_t3.x;
      position_t4.y = position_t3.y + 300;
      var scale_t4 = new THREE.Vector3();
      scale_t4.x = 280 * 8.5; // Math.random() * 200 + 100;

      scale_t4.y = 280 * 1; //Math.random() * 200 + 100;

      scale_t4.z = 280 * 1;
      matrix.compose(position_t4, quaternion, scale_t4);
      geometry_t4.applyMatrix(matrix);
      applyVertexColors(geometry_t4, color.setHex(0xFFFF00));
      var edges_t4 = new THREE.EdgesGeometry(geometry_t4);
      var line_t4 = new THREE.LineSegments(edges_t4, new THREE.LineBasicMaterial({
        color: 0x000000
      })); //this.scene.add(line_t4)
      //geometriesDrawn.push(geometry_t4);

      var geometry_tx = new THREE.BoxBufferGeometry();
      var position_tx = new THREE.Vector3();
      position_tx.x = this.data_array[0][0] + vehicle_width / 2;
      position_tx.y = this.data_array[0][1] - vehicle_width / 5;
      position_tx.z = this.data_array[0][2] + vehicle_length / 2 - 20 - leftShift;
      var scale_tx = new THREE.Vector3();
      scale_tx.x = vehicle_width + 1; // Math.random() * 200 + 100;

      scale_tx.y = vehicle_width / 10; //Math.random() * 200 + 100;

      scale_tx.z = vehicle_length + 1;
      matrix.compose(position_tx, quaternion, scale_tx);
      geometry_tx.applyMatrix(matrix);
      applyVertexColors(geometry_tx, color.setHex(0x696969));
      geometriesDrawn.push(geometry_tx);
      var edges_tx = new THREE.EdgesGeometry(geometry_tx);
      var line_tx = new THREE.LineSegments(edges_tx, new THREE.LineBasicMaterial({
        color: 0x000000
      }));
      this.scene.add(line_tx);
    }

    var objects = new THREE.Mesh(THREE.BufferGeometryUtils.mergeBufferGeometries(geometriesDrawn), defaultMaterial);
    this.scene.add(objects);
    this.renderer = new THREE.WebGLRenderer({
      antialias: true
    });
    this.renderer.setSize(this.container.clientWidth, this.container.clientHeight, true);

    if (this.container.childNodes && this.container.childNodes.length) {
      this.container.replaceChild(this.renderer.domElement, this.container.firstChild);
    } else {
      this.container.appendChild(this.renderer.domElement);
    }

    this.container.addEventListener('mousemove', onMouseMove.bind(this));
    var tanFOV = Math.tan(Math.PI / 180 * this.camera.fov / 2);
    var windowHeight = this.container.getBoundingClientRect().height;
  };

  QuifersPacking.prototype.animate = function () {
    requestAnimationFrame(this.boundAnimate);
    this.render();
  };

  QuifersPacking.prototype.render = function () {
    this.controls.update();

    if (this.renderer) {
      this.renderer.render(this.scene, this.camera);
    }
  };

  function onMouseMove(e) {
    this.mouse.x = e.clientX;
    this.mouse.y = e.clientY;
  }

  function onWindowResize(event) {
    this.camera.aspect = this.container.getBoundingClientRect().width / this.container.getBoundingClientRect().height; // adjust the FOV

    this.camera.fov = 360 / Math.PI * Math.atan(tanFOV * (this.container.getBoundingClientRect().width / this.container.getBoundingClientRect().height));
    this.camera.updateProjectionMatrix();
    this.camera.lookAt(this.scene.position);
    this.renderer.setSize(this.container.getBoundingClientRect().width, this.container.getBoundingClientRect().height);
    this.renderer.render(this.scene, this.camera);
  }

  exports.QuifersPacking = QuifersPacking;
});
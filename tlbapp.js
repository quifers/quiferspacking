define(function (require) {
	var QuifersPacking = require("qpacking").QuifersPacking;

	function loadJSON(callback) {
		var xobj = new XMLHttpRequest();
		xobj.overrideMimeType("application/json");
		xobj.open('GET', '/newCm.json', true); // Replace 'my_data' with the path to your file
		xobj.onreadystatechange = function () {
			if (xobj.readyState == 4 && xobj.status == "200") {
				// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
				callback(xobj.responseText);
			}
		};
		xobj.send(null);
	}

	loadJSON(function (response) {
		var actual_JSON = JSON.parse(response);
		new QuifersPacking(actual_JSON, document.getElementById("container1"), 25, false); // truck index no. 0
		//new QuifersPacking(actual_JSON, document.getElementById("container2"), 1, 25); // truck index no. 1
	});
});
